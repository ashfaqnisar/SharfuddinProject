
<?php
session_start();
include_once 'dbconnect.php';

if(isset($_SESSION['email'])!="")
{
	header("Location: en/dashboard.php");
}

if(isset($_POST['login']))
{
	$email = mysql_real_escape_string($_POST['email']);
	$password = mysql_real_escape_string($_POST['password']);
	
	$email = trim($email);
	$password = trim($password);
	
	$res=mysql_query("SELECT email, password FROM admin WHERE email='$email'");
	$row=mysql_fetch_array($res);
	
	$count = mysql_num_rows($res); // if Roll Number/Pass correct it returns must be 1 row
	
	if($count == 1 && $row['password']==($password))
	{
		$_SESSION['email'] = $row['email'];
		header("Location: en/dashboard.php");
	}
	else
	{
		?>
        <script>alert('Email / Password Seems Wrong !');</script>
        <?php
	}
	
}
?>



<!DOCTYPE html>
<html>
<head>
	<title>VJIT Student Docs - Admin Portal</title>
	<link rel="stylesheet" href="css/style.css">

	<link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<!-- For-Mobile-Apps-and-Meta-Tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="VJIT Student Docs, Sharing Resources Made Easy, Building Smart Relationship Made Easy, VJIT, Vidya Jyothi Institute of Techonology Student Docs" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //For-Mobile-Apps-and-Meta-Tags -->

</head>

<body>
    <h1>VJIT Student Docs</h1>
    <div class="container w3">
        <h2>Admin Portal</h2>
		<form action="#" method="post">
			<div class="username">
				<span class="username">Email:</span>
				<input type="email" name="email" class="name" placeholder="" required="">
				<div class="clear"></div>
			</div>
			<div class="password-agileits">
				<span class="username">Password:</span>
				<input type="password" name="password" class="password" placeholder="" required="">
				<div class="clear"></div>
			</div>
			
			<div class="login-w3">
					<input type="submit" name="login" class="login" value="Get In">
			</div>
			<div class="clear"></div>
		</form>
	</div>
	<div class="footer-w3l">
		<p> &copy; 2016 VJIT Student Docs. All Rights Reserved </p>
	</div>
</body>
</html>