<?php
session_start(); 
if (empty($_SESSION['staff_id']) && parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) != '/dashboard.php') {
    header('Location: ../../index.php');
    exit;
}

?>
<!DOCTYPE HTML>
<html>
<head>
<title>VJIT Student Docs | Staff Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="VJIT Student Docs - Sharing Resources Made Easy" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="js/jquery-2.1.1.min.js"></script> 
<!--icons-css-->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!--//skycons-icons-->
<style type="text/css">
.auto-style2 {
	font-size: larger;
}
</style>
</head>
<body>	
<div class="page-container">	
   <div class="left-content">
	   <div class="mother-grid-inner">
            <!--header start here-->
				<div class="header-main">
					<div class="header-left">
							<div class="logo-name">
									 <a href="dashboard.php"> 
									 <h1 class="auto-style2">Welcome, </h1> 
									<!--<img id="logo" src="" alt="Logo"/>--> 
								  </a> 								
							</div>
							
							<div class="clearfix"> </div>
						 </div>
						 <div class="header-right">
							
							<div class="profile_details">		
								<ul>
									<li class="dropdown profile_details_drop">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<div class="profile_img">	
												<span class="prfil-img">
												<img src="images/p1.png" alt="" class="auto-style2"><span class="auto-style2">
												</span> </span> 
												<div class="user-name">
													<p class="auto-style2"><?php echo $_SESSION["staff_id"]; ?></p>
													<span class="auto-style2">VJIT Staff </span>
												</div>
												<i class="fa fa-angle-down lnr"></i>
												<i class="fa fa-angle-up lnr"></i>
												<div class="clearfix"></div>	
											</div>	
										</a>
										<ul class="dropdown-menu drp-mnu">
											<li> 
											<a href="./settings.php" class="auto-style2"><i class="fa fa-cog"></i> Settings</a><span class="auto-style2">
											</span> </li> 
											<li> 
											<a href="./profile.php" class="auto-style2"><i class="fa fa-user"></i> Profile</a><span class="auto-style2">
											</span> </li> 
											<li> 
											<a href="./logout.php" class="auto-style2"><i class="fa fa-sign-out"></i> Logout</a><span class="auto-style2">
											</span> </li>
										</ul>
									</li>
								</ul>
							</div>
							<div class="clearfix"> </div>				
						</div>
				     <div class="clearfix"> </div>	
				</div>
<!--heder end here-->
<!-- script-for sticky-nav -->
		<script>
		$(document).ready(function() {
			 var navoffeset=$(".header-main").offset().top;
			 $(window).scroll(function(){
				var scrollpos=$(window).scrollTop(); 
				if(scrollpos >=navoffeset){
					$(".header-main").addClass("fixed");
				}else{
					$(".header-main").removeClass("fixed");
				}
			 });
			 
		});
		</script>
		<!-- /script-for sticky-nav -->
<!--inner block start here-->
<div class="inner-block">
    <div class="blank">
    	<h2 class="auto-style2">Upload Documents</h2>
    	<div class="blankpage-main">
    		<div class="col-sm-12 col-md-12 col-lg-12 mb-100">
                  
            <div class="horizontal-tab">
              <ul class="nav nav-tabs">
                <li class=""><a href="#cse" data-toggle="tab" aria-expanded="true">
				<button class="btn btn-lg btn-primary" style="font-size: larger">CSE</button></a></li>
                <li class=""><a href="#it" data-toggle="tab" aria-expanded="true">
				<button class="btn btn-lg btn-success" style="font-size: larger">IT</button></a></li>
                <li class=""><a href="#eee" data-toggle="tab" aria-expanded="true">
				<button class="btn btn-lg btn-warning" style="font-size: larger">EEE</button></a></li>
                <li class=""><a href="#ece" data-toggle="tab" aria-expanded="true">
				<button class="btn btn-lg btn-danger" style="font-size: larger">ECE</button></a></li>
                <li class=""><a href="#mech" data-toggle="tab" aria-expanded="true">
				<button class="btn btn-lg btn-info" style="font-size: larger">MECH</button></a></li>
                <li class=""><a href="#civil" data-toggle="tab" aria-expanded="true">
				<button class="btn btn-lg btn-deafult" style="font-size: larger">CIVIL</button></a></li>
              </ul>
              <div class="clearfix"> </div>
              <div class="tab-content">
                <div class="tab-pane" id="cse">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="grid1">
                     <a href="content/CSE/1/index.php"> 
					  <button class="btn btn-lg btn-primary" style="font-size: larger">1st Year</button></a>
                     <a href="content/CSE/2/index.php"> 
					  <button class="btn btn-lg btn-primary" style="font-size: larger">2nd Year</button></a>
                    <a href="content/CSE/3/index.php">  
					  <button class="btn btn-lg btn-primary" style="font-size: larger">3rd Year</button></a>
                  <a href="content/CSE/4/index.php">    
					  <button class="btn btn-lg btn-primary" style="font-size: larger">4th Year</button></a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="it">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="grid1">
                     <a href="content/IT/1/index.php"> 
					  <button class="btn btn-lg btn-success" style="font-size: larger">1st Year</button></a>
                     <a href="content/IT/2/index.php"> 
					  <button class="btn btn-lg btn-success" style="font-size: larger">2nd Year</button></a>
                     <a href="content/IT/3/index.php"> 
					  <button class="btn btn-lg btn-success" style="font-size: larger">3rd Year</button></a>
                     <a href="content/IT/4/index.php"> 
					  <button class="btn btn-lg btn-success" style="font-size: larger">4th Year</button></a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="eee">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="grid1">
                    <a href="content/EEE/1/index.php">  
					  <button class="btn btn-lg btn-warning" style="font-size: larger">1st Year</button></a>
                   <a href="content/EEE/2/index.php">   
					  <button class="btn btn-lg btn-warning" style="font-size: larger">2nd Year</button></a>
               <a href="content/EEE/3/index.php">       
					  <button class="btn btn-lg btn-warning" style="font-size: larger">3rd Year</button></a>
                   <a href="content/EEE/4/index.php">   
					  <button class="btn btn-lg btn-warning" style="font-size: larger">4th Year</button></a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="ece">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="grid1">
                  <a href="content/ECE/1/index.php">    
					  <button class="btn btn-lg btn-danger" style="font-size: larger">1st Year</button></a>
                       <a href="content/ECE/2/index.php">   
					  <button class="btn btn-lg btn-danger" style="font-size: larger">2nd Year</button></a>
                       <a href="content/ECE/3/index.php">   
					  <button class="btn btn-lg btn-danger" style="font-size: larger">3rd Year</button></a>
                       <a href="content/ECE/4/index.php">   
					  <button class="btn btn-lg btn-danger" style="font-size: larger">4th Year</button></a>
                      </p>
                    </div>
                  </div>
                </div>
                
                
                <div class="tab-pane" id="mech">
                  <div class="row">
                    <div class="col-md-12">
                       <p class="grid1">
                       <a href="content/MECH/1/index.php">   
					   <button class="btn btn-lg btn-info" style="font-size: larger">1st Year</button></a>
                     <a href="content/MECH/2/index.php">  
					   <button class="btn btn-lg btn-info" style="font-size: larger">2nd Year</button></a>
                       <a href="content/MECH/3/index.php">
					   <button class="btn btn-lg btn-info" style="font-size: larger">3rd Year</button></a>
                      <a href="content/MECH/4/index.php"> 
					   <button class="btn btn-lg btn-info" style="font-size: larger">4th Year</button></a>
                      </p>                    </div>
                  </div>
                </div>
                
                <div class="tab-pane" id="civil">
                  <div class="row">
                    <div class="col-md-12">
                       <p class="grid1">
                      <a href="content/CIVIL/1/index.php"> 
					   <button class="btn btn-lg btn-default" style="font-size: larger">1st Year</button></a>
                 <a href="content/CIVIL/2/index.php">     
					   <button class="btn btn-lg btn-default" style="font-size: larger">2nd Year</button></a>
                     <a href="content/CIVIL/3/index.php"> 
					   <button class="btn btn-lg btn-default" style="font-size: larger">3rd Year</button></a>
                  <a href="content/CIVIL/4/index.php">    
					   <button class="btn btn-lg btn-default" style="font-size: larger">4th Year</button></a>
                      </p>  
                    </div>
                  </div>
                </div>
                
                
              </div>
            </div>
          </div>
           <div class="clearfix"> </div>
          
          
          
          
          
    	</div>
    </div>
</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights">
	 <p class="auto-style2">© 2016 VJIT Student Docs. All Rights Reserved  </p>
</div>	
<!--COPY rights end here-->
</div>
</div>
	<span class="auto-style2">
<!--slider menu-->
    </span>
    <div class="sidebar-menu">
		  	<div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> 
				<span class="auto-style2"> <span id="logo" ></span> 
			      <!--<img id="logo" src="" alt="Logo"/>--> 
			    </span> 
			  </a> </div>		  
		    <div class="menu">
		      <ul id="menu" >
		        <li id="menu-home" >
				<a href="./dashboard.php" class="auto-style2"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
                <li id="menu-comunicacao" >
				<a href="./upload-document.php" class="auto-style2"><i class="fa fa-book nav_icon"></i><span>Upload Document</span></a></li>
                
		        <!-- <li><a href="#"><i class="fa fa-cogs"></i><span>Components</span><span class="fa fa-angle-right" style="float: right"></span></a>
		          <ul>
		            <li><a href="grids.html">Grids</a></li>
		            <li><a href="portlet.html">Portlets</a></li>		            
		          </ul>
		        </li> 
		        
		          <li><a href="maps.html"><i class="fa fa-map-marker"></i><span>Maps</span></a></li>
		        <li id="menu-academico" ><a href="#"><i class="fa fa-file-text"></i><span>Pages</span><span class="fa fa-angle-right" style="float: right"></span></a>
		          <ul id="menu-academico-sub" >
		          	 <li id="menu-academico-boletim" ><a href="login.html">Login</a></li>
		            <li id="menu-academico-avaliacoes" ><a href="signup.html">Sign Up</a></li>		           
		          </ul>
		        </li>
		        
		        <li><a href="charts.html"><i class="fa fa-bar-chart"></i><span>Charts</span></a></li>
		        <li><a href="#"><i class="fa fa-envelope"></i><span>Mailbox</span><span class="fa fa-angle-right" style="float: right"></span></a>
		        	 <ul id="menu-academico-sub" >
			            <li id="menu-academico-avaliacoes" ><a href="inbox.html">Inbox</a></li>
			            <li id="menu-academico-boletim" ><a href="inbox-details.html">Compose email</a></li>
		             </ul>
		        </li> -->
                <li><a href="./attendence.php" class="auto-style2"><i class="fa fa-bar-chart"></i><span>Upload Attendence</span></a></li>
                <li><a href="./profile.php" class="auto-style2"><i class="fa fa-user"></i><span>Profile</span></a><span class="auto-style2">
				</span>
		         	         </li>
                
		         <li><a href="./settings.php" class="auto-style2"><i class="fa fa-cog"></i><span>Settings</span></a><span class="auto-style2">
				 </span>
		         	         </li>
                             
                             <li><a href="./logout.php" class="auto-style2"><i class="fa fa-sign-out"></i><span>Log out!</span></a><span><span class="auto-style2">
							 </span>
		         	         </li>
		         
		      </ul>
		    </div>
	 </div>
	<div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
            });
</script>
<!--scrolling js-->
		<script src="js/jquery.nicescroll.js"></script>
		<script src="js/scripts.js"></script>
		<!--//scrolling js-->
<script src="js/bootstrap.js"> </script>
<!-- mother grid end here-->
</body>
</html>


                      
						
