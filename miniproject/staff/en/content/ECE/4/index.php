<?php
session_start(); 
if (empty($_SESSION['staff_id']) && parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) != '/dashboard.php') {
    header('Location: ../../../../../index.php');
    exit;
}

?>

<?php
include 'uploadconfig.php'
?>









<!DOCTYPE HTML>
<html>
<head>
<title>VJIT Student Docs | ECE Fourth Year</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="VJIT Student Docs - Sharing Resources Made Easy" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="js/jquery-2.1.1.min.js"></script> 
<!--icons-css-->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!--//skycons-icons-->

<style>
.demo {
		border:1px solid #C0C0C0;
		border-collapse:collapse;
		padding:5px;
	}
	.demo th {
		border:1px solid #C0C0C0;
		padding:5px;
		background:#F0F0F0;
		width:550px;
		color:#000000;
		
		}
	.demo td {
		border:1px solid #C0C0C0;
		padding:5px;
		color:#000000;
	}
    </style>





</head>
<body>	
<div class="page-container">	
   <div class="left-content">
	   <div class="mother-grid-inner">
            <!--header start here-->
				<div class="header-main">
					<div class="header-left">
							<div class="logo-name">
									 <a href="dashboard.php"> <h1>Welcome, </h1> 
									<!--<img id="logo" src="" alt="Logo"/>--> 
								  </a> 								
							</div>
							
							<div class="clearfix"> </div>
						 </div>
						 <div class="header-right">
							
							<div class="profile_details">		
								<ul>
									<li class="dropdown profile_details_drop">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<div class="profile_img">	
												<span class="prfil-img"><img src="images/p1.png" alt=""> </span> 
												<div class="user-name">
													<p><?php echo $_SESSION["staff_id"]; ?></p>
													<span>VJIT Staff</span>
												</div>
												<i class="fa fa-angle-down lnr"></i>
												<i class="fa fa-angle-up lnr"></i>
												<div class="clearfix"></div>	
											</div>	
										</a>
										<ul class="dropdown-menu drp-mnu">
											<li> <a href="../../../settings.php"><i class="fa fa-cog"></i> Settings</a> </li> 
											<li> <a href="../../../profile.php"><i class="fa fa-user"></i> Profile</a> </li> 
											<li> <a href="../../../logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
										</ul>
									</li>
								</ul>
							</div>
							<div class="clearfix"> </div>				
						</div>
				     <div class="clearfix"> </div>	
				</div>
<!--heder end here-->
<!-- script-for sticky-nav -->
		<script>
		$(document).ready(function() {
			 var navoffeset=$(".header-main").offset().top;
			 $(window).scroll(function(){
				var scrollpos=$(window).scrollTop(); 
				if(scrollpos >=navoffeset){
					$(".header-main").addClass("fixed");
				}else{
					$(".header-main").removeClass("fixed");
				}
			 });
			 
		});
		</script>
		<!-- /script-for sticky-nav -->
<!--inner block start here-->
<div class="inner-block">
    <div class="blank">
    	<h2>ECE - Fourth Year</h2>
    
    	
    	<center><div class="alert alert-success alert-dismissable">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">  </button>
            <h3> <?php
if (isset($_GET['m']{0}) && $_GET['m'] === 'success')
    echo 'File uploaded successfully';
?></h3></div>
</center>
    	

    	<div class="blankpage-main">
    		<div class="col-sm-12 col-md-12 col-lg-12 mb-100">
    		
    		
    		
    		
                  
            <div class="horizontal-tab">
              <ul class="nav nav-tabs">
                <li class=""><a href="#upload" data-toggle="tab" aria-expanded="true"><button type="button" class="btn btn-lg btn-primary">Upload Document</button></a></li>
                <li class=""><a href="#view" data-toggle="tab" aria-expanded="true"><button type="button" class="btn btn-lg btn-success">View Download Center</button></a></li>
                
              </ul>
              <div class="clearfix"> </div>
              <div class="tab-content">
                <div class="tab-pane" id="upload">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="grid1">
                      
                 

                   
                      <form enctype="multipart/form-data" action="" name="form" method="post">
                     <input type="file" name="photo" id="photo" class="btn btn-lg btn-primary" required=""/>
                     <br/>
                     <input type="submit" name="submit" id="submit" value="Upload" class="btn btn-lg btn-danger"/>
                    </form>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="view">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="grid1">
                      <table class="demo">
                      
                     
                      
                      
		<thead>
	<tr>
    
        <th><center><strong>File Name</strong></center></th>
		<th><center><strong>Download</strong></center></th>
	</tr>
	</thead>
	<tbody>
    
   
    
    
     <?php
$sqlCommand="select * from vjitstudentdocsece4 order by id desc";
$query=mysqli_query($myConnection, $sqlCommand) or die(mysql_error());
$menuDisplay="";
while($row1=mysqli_fetch_array($query)){
$name=$row1['name'];
?>

	<tr>
    
      
		<td><center><?php echo $name ;?></center></td>
		<td><center><a href="download.php?filename=<?php echo $name;?>"><img src="files/downloadfile.png" width="104" height="22"></a></center></td>
	</tr>
    
     
	</tbody>
    <?php }?>
    
    
    
</table>

         
                      </p>
                    </div>
                  </div>
                </div>
                
                
                
              </div>
            </div>
          </div>
           <div class="clearfix"> </div>
          
          
          
          
          
    	</div>
    </div>
</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights">
	 <p>© 2016 VJIT Student Docs. All Rights Reserved  </p>
</div>	
<!--COPY rights end here-->
</div>
</div>
<!--slider menu-->
    <div class="sidebar-menu">
		  	<div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span> 
			      <!--<img id="logo" src="" alt="Logo"/>--> 
			  </a> </div>		  
		    <div class="menu">
		      <ul id="menu" >
		        <li id="menu-home" ><a href="../../../dashboard.php"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
                <li id="menu-comunicacao" ><a href="../../../upload-document.php"><i class="fa fa-book nav_icon"></i><span>Upload Document</span></a></li>
                
		        <!-- <li><a href="#"><i class="fa fa-cogs"></i><span>Components</span><span class="fa fa-angle-right" style="float: right"></span></a>
		          <ul>
		            <li><a href="grids.html">Grids</a></li>
		            <li><a href="portlet.html">Portlets</a></li>		            
		          </ul>
		        </li> 
		        
		          <li><a href="maps.html"><i class="fa fa-map-marker"></i><span>Maps</span></a></li>
		        <li id="menu-academico" ><a href="#"><i class="fa fa-file-text"></i><span>Pages</span><span class="fa fa-angle-right" style="float: right"></span></a>
		          <ul id="menu-academico-sub" >
		          	 <li id="menu-academico-boletim" ><a href="login.html">Login</a></li>
		            <li id="menu-academico-avaliacoes" ><a href="signup.html">Sign Up</a></li>		           
		          </ul>
		        </li>
		        
		        <li><a href="charts.html"><i class="fa fa-bar-chart"></i><span>Charts</span></a></li>
		        <li><a href="#"><i class="fa fa-envelope"></i><span>Mailbox</span><span class="fa fa-angle-right" style="float: right"></span></a>
		        	 <ul id="menu-academico-sub" >
			            <li id="menu-academico-avaliacoes" ><a href="inbox.html">Inbox</a></li>
			            <li id="menu-academico-boletim" ><a href="inbox-details.html">Compose email</a></li>
		             </ul>
		        </li> -->
                <li><a href="../../../attendence.php"><i class="fa fa-bar-chart"></i><span>Upload Attendence</span></a></li>
                <li><a href="../../../profile.php"><i class="fa fa-user"></i><span>Profile</span></a>
		         	         </li>
                
		         <li><a href="../../../settings.php"><i class="fa fa-cog"></i><span>Settings</span></a>
		         	         </li>
                             
                             <li><a href="../../../logout.php"><i class="fa fa-sign-out"></i><span>Log out!<span></a>
		         	         </li>
		         
		      </ul>
		    </div>
	 </div>
	<div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
            });
</script>
<!--scrolling js-->
		<script src="js/jquery.nicescroll.js"></script>
		<script src="js/scripts.js"></script>
		<!--//scrolling js-->
<script src="js/bootstrap.js"> </script>
<!-- mother grid end here-->
</body>
</html>


                      
						
