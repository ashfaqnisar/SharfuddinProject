
<!DOCTYPE HTML>
<html>
<head>
<title>Student Portal | VJIT Students Docs</title>
<link href="css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="VJIT Student Docs - Sharing Resources Made Easy, Sharing, VJIT, VJIT Docs, Student Docs, Smart Relationship between student and faculty"./>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</script>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="all">
<!-- webfonts -->
	<link href='//fonts.googleapis.com/css?family=Lora|Cinzel+Decorative:400,700,900' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600' rel='stylesheet' type='text/css'>
<!-- webfonts-->
</head>
<body>
	<div class="hover-buttons">
		<div class="wrap">
			<div class="bg-effect">
				<h1>Select Your Department</h1>
				<ul class="bt-list">
					
						<li><a href="en/CSE/" class="hvr-icon-spin col-1">CSE</a></li>
						<li><a href="en/IT/" class="hvr-icon-spin col-2">IT</a></li>
						<li><a href="en/ECE/" class="hvr-icon-spin col-3">ECE</a></li>
						<li><a href="en/EEE/" class="hvr-icon-spin col-11">EEE</a></li>
				
					
						<li><a href="en/MECH/" class="hvr-icon-spin col-5">MECH</a></li>
						<li><a href="en/CIVIL/" class="hvr-icon-spin col-6">CIVIL</a></li>
						
					
					
				</ul>
			</div>
		</div>
	</div>
					<div class="copy-right">
							<p>Copyright &copy; 2016 VJIT Student Docs . All rights  Reserved </p>
					</div>	
	</body>
</html>