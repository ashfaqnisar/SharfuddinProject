<?php
session_start(); 
if (empty($_SESSION['eee3roll_no']) && parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) != '/dashboard.php') {
    header('Location: ../../../index.php');
    exit;
}
?>

<?php
 include_once 'download-db.php'
 ?>

<!DOCTYPE HTML>
<html>
<head>
<title>VJIT Student Docs | Download Documents</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="VJIT Student Docs - Sharing Resources Made Easy" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<script src="js/jquery.min.js"> </script>
<script src="js/bootstrap.min.js"> </script>
  
<!-- Mainly scripts -->
<script src="js/jquery.metisMenu.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<link href="css/custom.css" rel="stylesheet">
<script src="js/custom.js"></script>
<script src="js/screenfull.js"></script>
		<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});
			

			
		});
		</script>

<style>
.demo {
		border:1px solid #C0C0C0;
		border-collapse:collapse;
		padding:5px;
	}
	.demo th {
		border:1px solid #C0C0C0;
		padding:5px;
		background:#F0F0F0;
		width:550px;
		color:#000000;
		
		}
	.demo td {
		border:1px solid #C0C0C0;
		padding:5px;
		color:#000000;
	}
    </style>

</head>
<body>
<div id="wrapper">
       <!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="dashboard.php"><font size="5px">VJIT Student Docs</font></a></h1>         
			   </div>
			 <div class=" border-bottom">
        	<div class="full-left">
        	  <section class="full-top">
				<button id="toggle"><i class="fa fa-arrows-alt"></i></button>	
			</section>
			<form class=" navbar-left-right">
              <input type="text"  value="Search..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search...';}">
              <input type="submit" value="" class="fa fa-search">
            </form>
            <div class="clearfix"> </div>
           </div>
     
       
            <!-- Brand and toggle get grouped for better mobile display -->
		 
		   <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="drop-men" >
		        <ul class=" nav_1">
		           
		    		
					<li class="dropdown">
		              <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret"><?php echo $_SESSION['eee3roll_no'];?><i class="caret"></i></span><img src="images/p1.png"></a>
		              <ul class="dropdown-menu " role="menu">
		                <li><a href="profile.php"><i class="fa fa-user"></i>Profile</a></li>
                        
                        <li>
                        <a href="settings.php" ><i class="fa fa-cog"></i> Settings</a></li>
                        
                        
                        <li><a href="logout.php" ><i class="fa fa-sign-in"></i>Logout</a></li>
                        
                        
		                
		              </ul>
		            </li>
		           
		        </ul>
		     </div><!-- /.navbar-collapse -->
			<div class="clearfix">
       
     </div>
	  
		    <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
				
                    <li>
                        <a href="dashboard.php" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>
                    </li>
                   
                    <li><a href="download-documents.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>Download Documents</a></li>
                    
                    <li><a href="view-attendance.php" class=" hvr-bounce-to-right"><i class="fa fa-align-left nav_icon"></i>View Attendance</a></li>
                    
                    <li><a href="profile.php" class=" hvr-bounce-to-right"><i class="fa fa-user nav_icon"></i>Profile</a></li>
                    					 <li>
                        
                   <li>
                        <a href="settings.php" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Settings</span></a></li>
                        
                        
                        <li><a href="logout.php" class=" hvr-bounce-to-right"><i class="fa fa-sign-in nav_icon"></i>Logout</a></li>
                    
                   
                    
                </ul>
            </div>
			</div>
        </nav>
		 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="dashboard.php">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Download Documents</span>
				</h2>
		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="blank">
	

			<div class="blank-page">
				
	        	<center><h1>Download Documents</center>
                <br/>
                <br/>
                <table class="demo">
                      
                     
                      
                      
		<thead>
	<tr>
    
        <th><center><strong>File Name</strong></center></th>
		<th><center><strong>Download</strong></center></th>
	</tr>
	</thead>
	<tbody>
    
   
    
    
     <?php
$sqlCommand="select * from vjitstudentdocseee3 order by id desc";
$query=mysqli_query($myConnection, $sqlCommand) or die(mysql_error());
$menuDisplay="";
while($row1=mysqli_fetch_array($query)){
$name=$row1['name'];
?>

	<tr>
    
      
		<td><center><?php echo $name ;?></center></td>
		<td><center><a href="../../../../staff\en\content\EEE\3\download.php?filename=<?php echo $name;?>"><img src="../../../../staff\en\content\EEE\3\files/downloadfile.png" width="104" height="22"></a></center></td>
	</tr>
    
     
	</tbody>
    <?php }?>
    
    
    
</table>

            
                 <div class="clearfix"> </div>
	        </div>
            
            
            
	       </div>
          
	
	<!--//faq-->
		<!---->
<div class="copy">
            <p> &copy; 2016 VJIT Student Docs. All Rights Reserved  </p>	    </div>
		</div>
		</div>
		<div class="clearfix"> </div>
       </div>
     
<!---->
<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
</body>
</html>

