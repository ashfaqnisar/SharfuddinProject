-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2016 at 08:51 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registrations`
--

-- --------------------------------------------------------

--
-- Table structure for table `staffdetails`
--

CREATE TABLE `staffdetails` (
  `user_id` int(11) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `e_name` varchar(35) NOT NULL,
  `e_email` varchar(35) NOT NULL,
  `e_pass` varchar(255) NOT NULL,
  `e_pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffdetails`
--

INSERT INTO `staffdetails` (`user_id`, `staff_id`, `e_name`, `e_email`, `e_pass`, `e_pin`) VALUES
(1, 'VJIT5859', 'Manisha Maheshkar', 'manisha.maheshkar@gmail.com', 'ef764106d880d6a248ba2359d38128f9', '5859');

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailscivil1`
--

CREATE TABLE `studentdetailscivil1` (
  `user_id` int(11) NOT NULL,
  `civil1roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studentdetailscivil1`
--

INSERT INTO `studentdetailscivil1` (`user_id`, `civil1roll_no`, `user_name`, `user_email`, `user_pass`, `pin`) VALUES
(1, 'VJITCIVIL1', 'Mohammad Sharfuddin', 'mohammadsharfuddin@outlook.in', 'e10adc3949ba59abbe56e057f20f883e', '5859');

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailscivil2`
--

CREATE TABLE `studentdetailscivil2` (
  `user_id` int(11) NOT NULL,
  `civil2roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailscivil3`
--

CREATE TABLE `studentdetailscivil3` (
  `user_id` int(11) NOT NULL,
  `civil3roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailscivil4`
--

CREATE TABLE `studentdetailscivil4` (
  `user_id` int(11) NOT NULL,
  `civil4roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailscse1`
--

CREATE TABLE `studentdetailscse1` (
  `user_id` int(11) NOT NULL,
  `cse1roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailscse2`
--

CREATE TABLE `studentdetailscse2` (
  `user_id` int(11) NOT NULL,
  `cse2roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailscse3`
--

CREATE TABLE `studentdetailscse3` (
  `user_id` int(11) NOT NULL,
  `cse3roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailscse4`
--

CREATE TABLE `studentdetailscse4` (
  `user_id` int(11) NOT NULL,
  `cse4roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsece1`
--

CREATE TABLE `studentdetailsece1` (
  `user_id` int(11) NOT NULL,
  `ece1roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsece2`
--

CREATE TABLE `studentdetailsece2` (
  `user_id` int(11) NOT NULL,
  `ece2roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsece3`
--

CREATE TABLE `studentdetailsece3` (
  `user_id` int(11) NOT NULL,
  `ece3roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsece4`
--

CREATE TABLE `studentdetailsece4` (
  `user_id` int(11) NOT NULL,
  `ece4roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailseee1`
--

CREATE TABLE `studentdetailseee1` (
  `user_id` int(11) NOT NULL,
  `eee1roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailseee2`
--

CREATE TABLE `studentdetailseee2` (
  `user_id` int(11) NOT NULL,
  `eee2roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailseee3`
--

CREATE TABLE `studentdetailseee3` (
  `user_id` int(11) NOT NULL,
  `eee3roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailseee4`
--

CREATE TABLE `studentdetailseee4` (
  `user_id` int(11) NOT NULL,
  `eee4roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsit1`
--

CREATE TABLE `studentdetailsit1` (
  `user_id` int(11) NOT NULL,
  `it1roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsit2`
--

CREATE TABLE `studentdetailsit2` (
  `user_id` int(11) NOT NULL,
  `it2roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsit3`
--

CREATE TABLE `studentdetailsit3` (
  `user_id` int(11) NOT NULL,
  `it3roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsit4`
--

CREATE TABLE `studentdetailsit4` (
  `user_id` int(11) NOT NULL,
  `it4roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsmech1`
--

CREATE TABLE `studentdetailsmech1` (
  `user_id` int(11) NOT NULL,
  `mech1roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsmech2`
--

CREATE TABLE `studentdetailsmech2` (
  `user_id` int(11) NOT NULL,
  `mech2roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsmech3`
--

CREATE TABLE `studentdetailsmech3` (
  `user_id` int(11) NOT NULL,
  `mech3roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentdetailsmech4`
--

CREATE TABLE `studentdetailsmech4` (
  `user_id` int(11) NOT NULL,
  `mech4roll_no` varchar(10) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `staffdetails`
--
ALTER TABLE `staffdetails`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `staff_id` (`staff_id`),
  ADD UNIQUE KEY `e_email` (`e_email`);

--
-- Indexes for table `studentdetailscivil1`
--
ALTER TABLE `studentdetailscivil1`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailscivil2`
--
ALTER TABLE `studentdetailscivil2`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailscivil3`
--
ALTER TABLE `studentdetailscivil3`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailscivil4`
--
ALTER TABLE `studentdetailscivil4`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailscse1`
--
ALTER TABLE `studentdetailscse1`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailscse2`
--
ALTER TABLE `studentdetailscse2`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailscse3`
--
ALTER TABLE `studentdetailscse3`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailscse4`
--
ALTER TABLE `studentdetailscse4`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsece1`
--
ALTER TABLE `studentdetailsece1`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsece2`
--
ALTER TABLE `studentdetailsece2`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsece3`
--
ALTER TABLE `studentdetailsece3`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsece4`
--
ALTER TABLE `studentdetailsece4`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailseee1`
--
ALTER TABLE `studentdetailseee1`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailseee2`
--
ALTER TABLE `studentdetailseee2`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailseee3`
--
ALTER TABLE `studentdetailseee3`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailseee4`
--
ALTER TABLE `studentdetailseee4`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsit1`
--
ALTER TABLE `studentdetailsit1`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsit2`
--
ALTER TABLE `studentdetailsit2`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsit3`
--
ALTER TABLE `studentdetailsit3`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsit4`
--
ALTER TABLE `studentdetailsit4`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsmech1`
--
ALTER TABLE `studentdetailsmech1`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsmech2`
--
ALTER TABLE `studentdetailsmech2`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsmech3`
--
ALTER TABLE `studentdetailsmech3`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `studentdetailsmech4`
--
ALTER TABLE `studentdetailsmech4`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `staffdetails`
--
ALTER TABLE `staffdetails`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `studentdetailscivil1`
--
ALTER TABLE `studentdetailscivil1`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `studentdetailscivil2`
--
ALTER TABLE `studentdetailscivil2`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailscivil3`
--
ALTER TABLE `studentdetailscivil3`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailscivil4`
--
ALTER TABLE `studentdetailscivil4`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailscse1`
--
ALTER TABLE `studentdetailscse1`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailscse2`
--
ALTER TABLE `studentdetailscse2`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailscse3`
--
ALTER TABLE `studentdetailscse3`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailscse4`
--
ALTER TABLE `studentdetailscse4`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsece1`
--
ALTER TABLE `studentdetailsece1`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsece2`
--
ALTER TABLE `studentdetailsece2`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsece3`
--
ALTER TABLE `studentdetailsece3`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsece4`
--
ALTER TABLE `studentdetailsece4`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailseee1`
--
ALTER TABLE `studentdetailseee1`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailseee2`
--
ALTER TABLE `studentdetailseee2`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailseee3`
--
ALTER TABLE `studentdetailseee3`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailseee4`
--
ALTER TABLE `studentdetailseee4`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsit1`
--
ALTER TABLE `studentdetailsit1`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsit2`
--
ALTER TABLE `studentdetailsit2`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsit3`
--
ALTER TABLE `studentdetailsit3`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsit4`
--
ALTER TABLE `studentdetailsit4`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsmech1`
--
ALTER TABLE `studentdetailsmech1`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsmech2`
--
ALTER TABLE `studentdetailsmech2`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsmech3`
--
ALTER TABLE `studentdetailsmech3`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `studentdetailsmech4`
--
ALTER TABLE `studentdetailsmech4`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
