-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2016 at 08:52 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vjitstudentdocs`
--

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocscivil1`
--

CREATE TABLE `vjitstudentdocscivil1` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vjitstudentdocscivil1`
--

INSERT INTO `vjitstudentdocscivil1` (`id`, `name`) VALUES
(1, 'Welcome.png');

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocscivil2`
--

CREATE TABLE `vjitstudentdocscivil2` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocscivil3`
--

CREATE TABLE `vjitstudentdocscivil3` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocscivil4`
--

CREATE TABLE `vjitstudentdocscivil4` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocscse1`
--

CREATE TABLE `vjitstudentdocscse1` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vjitstudentdocscse1`
--

INSERT INTO `vjitstudentdocscse1` (`id`, `name`) VALUES
(1, 'Welcome.png');

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocscse2`
--

CREATE TABLE `vjitstudentdocscse2` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocscse3`
--

CREATE TABLE `vjitstudentdocscse3` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocscse4`
--

CREATE TABLE `vjitstudentdocscse4` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsece1`
--

CREATE TABLE `vjitstudentdocsece1` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsece2`
--

CREATE TABLE `vjitstudentdocsece2` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsece3`
--

CREATE TABLE `vjitstudentdocsece3` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsece4`
--

CREATE TABLE `vjitstudentdocsece4` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocseee1`
--

CREATE TABLE `vjitstudentdocseee1` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocseee2`
--

CREATE TABLE `vjitstudentdocseee2` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocseee3`
--

CREATE TABLE `vjitstudentdocseee3` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocseee4`
--

CREATE TABLE `vjitstudentdocseee4` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsit1`
--

CREATE TABLE `vjitstudentdocsit1` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsit2`
--

CREATE TABLE `vjitstudentdocsit2` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsit3`
--

CREATE TABLE `vjitstudentdocsit3` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsit4`
--

CREATE TABLE `vjitstudentdocsit4` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsmech1`
--

CREATE TABLE `vjitstudentdocsmech1` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsmech2`
--

CREATE TABLE `vjitstudentdocsmech2` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsmech3`
--

CREATE TABLE `vjitstudentdocsmech3` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjitstudentdocsmech4`
--

CREATE TABLE `vjitstudentdocsmech4` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vjitstudentdocscivil1`
--
ALTER TABLE `vjitstudentdocscivil1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocscivil2`
--
ALTER TABLE `vjitstudentdocscivil2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocscivil3`
--
ALTER TABLE `vjitstudentdocscivil3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocscivil4`
--
ALTER TABLE `vjitstudentdocscivil4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocscse1`
--
ALTER TABLE `vjitstudentdocscse1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocscse2`
--
ALTER TABLE `vjitstudentdocscse2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocscse3`
--
ALTER TABLE `vjitstudentdocscse3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocscse4`
--
ALTER TABLE `vjitstudentdocscse4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsece1`
--
ALTER TABLE `vjitstudentdocsece1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsece2`
--
ALTER TABLE `vjitstudentdocsece2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsece3`
--
ALTER TABLE `vjitstudentdocsece3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsece4`
--
ALTER TABLE `vjitstudentdocsece4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocseee1`
--
ALTER TABLE `vjitstudentdocseee1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocseee2`
--
ALTER TABLE `vjitstudentdocseee2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocseee3`
--
ALTER TABLE `vjitstudentdocseee3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocseee4`
--
ALTER TABLE `vjitstudentdocseee4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsit1`
--
ALTER TABLE `vjitstudentdocsit1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsit2`
--
ALTER TABLE `vjitstudentdocsit2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsit3`
--
ALTER TABLE `vjitstudentdocsit3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsit4`
--
ALTER TABLE `vjitstudentdocsit4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsmech1`
--
ALTER TABLE `vjitstudentdocsmech1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsmech2`
--
ALTER TABLE `vjitstudentdocsmech2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsmech3`
--
ALTER TABLE `vjitstudentdocsmech3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vjitstudentdocsmech4`
--
ALTER TABLE `vjitstudentdocsmech4`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vjitstudentdocscivil1`
--
ALTER TABLE `vjitstudentdocscivil1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vjitstudentdocscivil2`
--
ALTER TABLE `vjitstudentdocscivil2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocscivil3`
--
ALTER TABLE `vjitstudentdocscivil3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocscivil4`
--
ALTER TABLE `vjitstudentdocscivil4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocscse1`
--
ALTER TABLE `vjitstudentdocscse1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vjitstudentdocscse2`
--
ALTER TABLE `vjitstudentdocscse2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocscse3`
--
ALTER TABLE `vjitstudentdocscse3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocscse4`
--
ALTER TABLE `vjitstudentdocscse4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsece1`
--
ALTER TABLE `vjitstudentdocsece1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsece2`
--
ALTER TABLE `vjitstudentdocsece2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsece3`
--
ALTER TABLE `vjitstudentdocsece3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsece4`
--
ALTER TABLE `vjitstudentdocsece4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocseee1`
--
ALTER TABLE `vjitstudentdocseee1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocseee2`
--
ALTER TABLE `vjitstudentdocseee2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocseee3`
--
ALTER TABLE `vjitstudentdocseee3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocseee4`
--
ALTER TABLE `vjitstudentdocseee4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsit1`
--
ALTER TABLE `vjitstudentdocsit1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsit2`
--
ALTER TABLE `vjitstudentdocsit2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsit3`
--
ALTER TABLE `vjitstudentdocsit3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsit4`
--
ALTER TABLE `vjitstudentdocsit4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsmech1`
--
ALTER TABLE `vjitstudentdocsmech1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsmech2`
--
ALTER TABLE `vjitstudentdocsmech2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsmech3`
--
ALTER TABLE `vjitstudentdocsmech3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vjitstudentdocsmech4`
--
ALTER TABLE `vjitstudentdocsmech4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
